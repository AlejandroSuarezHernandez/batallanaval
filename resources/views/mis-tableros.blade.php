@extends('layout.main')

@section('titulo')
    <title>Mis Tablero | Batalla Naval</title>
@endsection

@section('css')

@endsection
<style type="text/css">
    .barco{
        margin-top: 2%;
    }
</style>
@section('titulo-pagina')
    <h1 class="h3 mb-4 text-gray-800">Mis Tableros:</h1>
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tableros de: {{session('usuario')->correo}}
        </h6>
    </div>
    <p class="mb-4">Instrucciones de juego
    </p>

    <!-- Content Row -->
    <div class="row">

        <!-- Second Column -->
        <div class="col-lg-8 offset-1">

            <!-- Background Gradient Utilities -->
            <div class="card shadow mb-8">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID_Tablero</th>

                            <th>Codigo</th>

                            <th>ID_Usuario_1</th>

                            <th>ID_Usuario_2</th>

                            <th>Esatus</th>

                            <th>Ganador</th>

                            <th>Creacion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <?php
                            echo "<tr>";
                            foreach ($tablero as $tb) {
                                echo "<td>$tb->id</td>";
                                echo "<td>$tb->codigo</td>";
                                echo "<td>$tb->usuario1_id</td>";
                                echo "<td>$tb->usuario2_id</td>";
                                echo "<td>$tb->estatus</td>";
                                echo "<td>$tb->ganador_id</td>";
                                echo "<td>$tb->created_at</td>";
                                echo "</tr>";
                            }
                        ?>
                    </tbody>
                </table>


                <!--
                <div class="card-body">
                    <button valorBarco="1" class="barco btn-barco px-2 py-3 bg-gradient-danger text-white">.bg-gradient-dark</button>
                    <button valorBarco="2" class="barco btn-barco px-2 py-3 bg-gradient-dark text-white">.bg-gradient-dark</button>
                    <button valorBarco="3" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="4" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="5" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="6" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="7" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="8" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="9" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="10" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="11" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarco="12" class="barco btn-barco px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                </div>
                -->
            </div>
        </div>
        <!-- Second Column -->
        <!-- Second Column -->
        <!--
        <div class="col-lg-4 offset-2">

             Background Gradient Utilities
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Tablero de jugador: <b>hola</b>
                    </h6>
                </div>
                <div class="card-body">

                    <button valorBarcoEnemigo="1" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-danger text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="2" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-dark text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="3" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="4" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="5" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="6" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="7" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="8" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="9" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="10" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="11" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                    <button valorBarcoEnemigo="12" class="barco btn-barco-enemigo px-2 py-3 bg-gradient-primary text-white">.bg-gradient-dark</button>
                </div>
            </div>
        </div>
    -->
    </div>
@endsection

@section('contenido')

@endsection

@section('js')
    <script>
        $(document).ready(function (){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "get",
                url: "{{route('usuario.peticion')}}",
                dataType: 'json',
                cache: false,
                success: function (data) {
                    console.log(data);
                }
            });

            $(".btn-barco").click(function (){
                var idBarco = $(this).attr('valorBarco');
               //alert($(this).attr('valorBarco'));
                if(idBarco == 12)
                    alert(":D");


            });
        });
    </script>
@endsection

